﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestChat.Infrastructure
{
    /// <summary>
    /// Сообщение
    /// </summary>
    public class Message
    {
        /// <summary>
        /// Пользователь
        /// </summary>
        public string User { get; set; }

        /// <summary>
        /// Текст сообщения
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Комната
        /// </summary>
        public string Room { get; set; }

        /// <summary>
        /// Время прихода на сервер
        /// </summary>
        public DateTime? DateTime { get; set; }
    }
}
