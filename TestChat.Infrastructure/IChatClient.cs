﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestChat.Infrastructure
{
    /// <summary>
    /// Клиент
    /// </summary>
    public interface IChatClient
    {
        /// <summary>
        /// Callback, вызываемый сервером для передачи сообщений клиенту
        /// </summary>
        /// <param name="message">Сообщение</param>
        void AddNewMessage(Message message);
    }
}
