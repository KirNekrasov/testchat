﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestChat.Infrastructure
{
    /// <summary>
    /// Сервер
    /// </summary>
    public interface IChatService
    {
        /// <summary>
        /// Присоединиться к комнате
        /// </summary>
        /// <param name="room">Комната</param>
        /// <param name="user">Пользователь</param>
        /// <returns></returns>
        Task JoinRoomAsync(string room, string user);

        /// <summary>
        /// Покинуть комнату
        /// </summary>
        /// <param name="room">Комната</param>
        /// <returns></returns>
        Task LeaveRoomAsync(string room);

        /// <summary>
        /// Отправить сообщение
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <returns></returns>
        Task SendAsync(Message message);

        /// <summary>
        /// Получить сообщения из комнаты от заданной даты
        /// </summary>
        /// <param name="room">Комната</param>
        /// <param name="from">Дата</param>
        /// <returns></returns>
        Task<IEnumerable<Message>> GetLastMessagesAsync(string room, DateTime from);

        /// <summary>
        /// Получить сообщения из комнаты за сегодня
        /// </summary>
        /// <param name="room">Комната</param>
        /// <returns></returns>
        Task<IEnumerable<Message>> GetTodayMessagesAsync(string room);
    }
}
