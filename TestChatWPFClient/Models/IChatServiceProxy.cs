﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestChat.Infrastructure;

namespace TestChatWPFClient.Models
{
    public interface IChatServiceProxy : IChatService
    {
        Task<bool> TryConnectAsync(string host);
    }
}
