﻿using Microsoft.AspNet.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestChat.Infrastructure;

namespace TestChatWPFClient.Models
{
    public class ChatHubService : IChatServiceProxy
    {
        private IChatClient client;

        private IHubProxy chatHubProxy;

        private string host;

        private bool isConnected;


        public ChatHubService(IChatClient client)
        {
            if (client == null)
            {
                throw new ArgumentNullException();
            }

            this.client = client;
        }


        public async Task<bool> TryConnectAsync(string host)
        {
            Uri uriResult;
            bool isValid =
                Uri.TryCreate(host, UriKind.Absolute, out uriResult)
                && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);

            if (!isValid)
            {
                return false;
            }

            this.host = host;

            var hubConnection = new HubConnection(this.host);

            this.chatHubProxy = hubConnection.CreateHubProxy("ChatHub");
            chatHubProxy.On<Message>("AddNewMessage", this.client.AddNewMessage);

            await hubConnection.Start();

            this.isConnected = hubConnection.State == ConnectionState.Connected;

            return this.isConnected;
        }

        public async Task JoinRoomAsync(string room, string user)
        {
            if (this.isConnected)
            {
                await this.chatHubProxy.Invoke("JoinRoomAsync", room, user);
            }
        }

        public async Task LeaveRoomAsync(string room)
        {
            if (this.isConnected)
            {
                await this.chatHubProxy.Invoke("LeaveRoomAsync", room);
            }
        }

        public async Task SendAsync(Message message)
        {
            if (this.isConnected)
            {
                await this.chatHubProxy.Invoke("SendAsync", message);
            }
        }

        public async Task<IEnumerable<Message>> GetLastMessagesAsync(string room, DateTime from)
        {
            if (this.isConnected)
            {
                return await this.chatHubProxy.Invoke<IEnumerable<Message>>("GetLastMessagesAsync", room, from);
            }
            else
            {
                return new Message[0];
            }
        }

        public async Task<IEnumerable<Message>> GetTodayMessagesAsync(string room)
        {
            return await this.GetLastMessagesAsync(room, DateTime.UtcNow.Date);
        }
    }
}
