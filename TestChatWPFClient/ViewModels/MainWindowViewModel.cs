﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using TestChat.Infrastructure;
using TestChatWPFClient.Models;
using TestChatWPFClient.ViewModels.Commands;

namespace TestChatWPFClient.ViewModels
{
    class MainWindowViewModel : ViewModelBase, IChatClient
    {
        private const string defaultRoom = "default";

        private const string defaultHost = "http://localhost:57362/";

        private string roomConnected;

        private string nameConnected;


        public MainWindowViewModel()
        {
            this.ConnectAsyncCommand = new DelegateCommandAsync(this.ConnectAsync, this.CanConnect);
            this.JoinRoomAsyncCommand = new DelegateCommandAsync(this.JoinRoomAsync, this.CanJoinRoom);
            this.SendMessageAsyncCommand = new DelegateCommandAsync(this.SendAsync, this.CanSend);

            this.Room = defaultRoom;
            this.Host = defaultHost;
            this.History = new ObservableCollection<ChatMessageViewModel>();
        }


        public ICommandAsync ConnectAsyncCommand { get; private set; }

        private async Task ConnectAsync()
        {
            this.IsConnected = false;
            this.History = new ObservableCollection<ChatMessageViewModel>();

            await this.LeaveRoomAsync();

            this.IsConnected = await this.Service.TryConnectAsync(this.Host);
            if (!this.IsConnected)
            {
                MessageBox.Show("Connection error!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
        }

        private async Task LeaveRoomAsync()
        {
            if (this.roomConnected != null)
            {
                await this.Service.LeaveRoomAsync(this.roomConnected);
                this.roomConnected = null;
                this.nameConnected = null;
                this.IsInRoom = false;
            }
        }

        private bool CanConnect()
        {
            return !String.IsNullOrEmpty(this.Host) && (this.Service != null);
        }


        public ICommandAsync JoinRoomAsyncCommand { get; private set; }

        private async Task JoinRoomAsync()
        {
            await this.LeaveRoomAsync();

            var tmpRoom = this.Room;
            var tmpName = this.Name;

            await this.Service.JoinRoomAsync(tmpRoom, tmpName);

            this.roomConnected = tmpRoom;
            this.nameConnected = tmpName;

            this.IsInRoom = true;

            var lastMessages = await this.Service.GetTodayMessagesAsync(this.roomConnected);
            this.History = new ObservableCollection<ChatMessageViewModel>(
                    lastMessages.Select(message => new ChatMessageViewModel(message)));
        }

        private bool CanJoinRoom()
        {
            return this.IsConnected && !String.IsNullOrEmpty(this.Room) 
                && !String.IsNullOrEmpty(this.Name);
        }


        public ICommandAsync SendMessageAsyncCommand { get; private set; }

        private async Task SendAsync()
        {
            await this.Service.SendAsync(
                new Message() {
                    Room = this.roomConnected,
                    User = this.nameConnected,
                    Text = this.Message
                });
            this.Message = null;
        }

        private bool CanSend()
        {
            return this.IsInRoom && !String.IsNullOrEmpty(this.Message);
        }


        private IChatServiceProxy service;

        public IChatServiceProxy Service
        {
            get
            {
                return this.service;
            }
            set
            {
                this.service = value;
                this.NotifyPropertyChanged();
                this.ConnectAsyncCommand.NotifyCanExecuteChanged();
            }
        }

        private bool isConnected;

        public bool IsConnected
        {
            get
            {
                return this.isConnected;
            }
            set
            {
                this.isConnected = value;
                this.NotifyPropertyChanged();
                this.JoinRoomAsyncCommand.NotifyCanExecuteChanged();
                this.SendMessageAsyncCommand.NotifyCanExecuteChanged();
            }
        }

        private bool isInRoom;

        public bool IsInRoom
        {
            get
            {
                return this.isInRoom;
            }
            set
            {
                this.isInRoom = value;
                this.NotifyPropertyChanged();
                this.SendMessageAsyncCommand.NotifyCanExecuteChanged();
            }
        }

        private ObservableCollection<ChatMessageViewModel> history;

        public ObservableCollection<ChatMessageViewModel> History
        {
            get
            {
                return this.history;
            }
            set
            {
                this.history = value;
                this.NotifyPropertyChanged();
            }
        }

        private string host;

        public string Host
        {
            get
            {
                return this.host;
            }
            set
            {
                this.host = value;
                this.NotifyPropertyChanged();
                this.ConnectAsyncCommand.NotifyCanExecuteChanged();
            }
        }

        private string name;

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
                this.NotifyPropertyChanged();
                this.JoinRoomAsyncCommand.NotifyCanExecuteChanged();
            }
        }

        private string message;

        public string Message
        {
            get
            {
                return this.message;
            }
            set
            {
                this.message = value;
                this.NotifyPropertyChanged();
                this.SendMessageAsyncCommand.NotifyCanExecuteChanged();
            }
        }

        private string room;

        public string Room
        {
            get
            {
                return this.room;
            }
            set
            {
                this.room = value;
                this.NotifyPropertyChanged();
                this.JoinRoomAsyncCommand.NotifyCanExecuteChanged();
            }
        }


        public void AddNewMessage(Message message)
        {
            Application.Current.Dispatcher.InvokeAsync(
                () =>
                    this.History.Add(
                        new ChatMessageViewModel()
                        {
                            DateTime = message.DateTime ?? DateTime.MinValue,
                            Name = message.User,
                            Message = message.Text
                        }));
        }
    }
}
