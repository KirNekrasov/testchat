﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestChat.Infrastructure;

namespace TestChatWPFClient.ViewModels
{
    class ChatMessageViewModel
    {
        public ChatMessageViewModel()
        {

        }

        public ChatMessageViewModel(Message message)
        {
            this.Message = message.Text;
            this.Name = message.User;
            this.DateTime = message.DateTime?.ToLocalTime() ?? DateTime.MinValue;
        }

        public string Message { get; set; }

        public string Name { get; set; }

        public DateTime DateTime { get; set; }
    }
}
