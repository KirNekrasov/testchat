﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestChatWPFClient.ViewModels.Commands
{
    public class DelegateCommandAsync : DelegateCommandBase, ICommandAsync
    {
        protected readonly Func<Task> executeAsync;


        public DelegateCommandAsync(Func<Task> executeAsync)
            : this(executeAsync, null)
        {

        }

        public DelegateCommandAsync(Func<Task> executeAsync, Func<bool> predicate)
            : base(predicate)
        {
            if (executeAsync == null)
            {
                throw new ArgumentNullException(nameof(executeAsync));
            }

            this.executeAsync = executeAsync;
        }


        public override async void Execute(object parameter)
        {
            if (this.CanExecute(parameter))
            {
                await this.ExecuteAsync(parameter);
            }
        }

        public async Task ExecuteAsync(object parameter)
        {
            if (this.CanExecute(parameter))
            {
                await this.executeAsync();
            }
        }
    }
}
