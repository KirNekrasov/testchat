﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestChatWPFClient.ViewModels.Commands
{
    public class DelegateCommand : DelegateCommandBase
    {
        protected readonly Action execute;


        public DelegateCommand(Action execute)
            : this(execute, null)
        {

        }

        public DelegateCommand(Action execute, Func<bool> canExecute)
            : base(canExecute)
        {
            if (execute == null)
            {
                throw new ArgumentNullException(nameof(execute));
            }

            this.execute = execute;
        }

        public override void Execute(object parameter)
        {
            if (CanExecute(parameter))
            {
                this.execute();
            }
        }
    }
}
