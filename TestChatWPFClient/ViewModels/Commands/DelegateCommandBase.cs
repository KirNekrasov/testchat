﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace TestChatWPFClient.ViewModels.Commands
{
    public abstract class DelegateCommandBase : ICommand
    {
        protected readonly Func<bool> canExecute;


        public DelegateCommandBase(Func<bool> canExecute = null)
        {
            this.canExecute = canExecute;
        }


        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }

            remove { CommandManager.RequerySuggested -= value; }
        }

        protected virtual void OnCanExecuteChanged()
        {
            CommandManager.InvalidateRequerySuggested();
        }


        public bool CanExecute(object parameter)
        {
            return this.canExecute?.Invoke() ?? true;
        }


        public abstract void Execute(object parameter);


        public void NotifyCanExecuteChanged()
        {
            this.OnCanExecuteChanged();
        }
    }
}
