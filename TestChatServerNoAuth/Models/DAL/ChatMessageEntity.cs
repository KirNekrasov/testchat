﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestChat.Infrastructure;

namespace TestChatServerNoAuth.Models.DAL
{
    public class ChatMessageEntity : TableEntity
    {
        public ChatMessageEntity(Message message)
        {
            this.PartitionKey = message.Room;
            this.RowKey = message.DateTime?.ToString();
            this.User = message.User;
            this.Text = message.Text;
        }

        public ChatMessageEntity(string room, DateTime datetime)
        {
            this.PartitionKey = room;
            this.RowKey = datetime.ToString();
        }

        public ChatMessageEntity()
        {

        }

        public Message CreateMessage()
        {
            return new Message()
            {
                Text = this.Text,
                User = this.User,
                Room = this.PartitionKey,
                DateTime = DateTime.Parse(this.RowKey)
            };
        }


        public string User { get; set; }

        public string Text { get; set; }
    }
}
