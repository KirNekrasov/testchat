﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.WindowsAzure.Storage.Table;
using TestChat.Infrastructure;
using System.Threading.Tasks;

namespace TestChatServerNoAuth.Models.DAL
{
    /// <summary>
    /// Azure Table Storage репозиторий
    /// </summary>
    /// <remarks>
    /// В качестве ключа раздела выбрана комната,
    /// в качестве ключа строки выбрана UTC время прихода сообщения на сервер
    /// </remarks>
    class MessagesRepository : IMessagesRepository
    {
        private CloudTable table;


        public MessagesRepository(CloudTable table)
        {
            if (table == null)
            {
                throw new ArgumentNullException("table");
            }

            this.table = table;
        }


        public async Task Add(Message entity)
        {
            var insertion = TableOperation.Insert(new ChatMessageEntity(entity));

            await table.ExecuteAsync(insertion);
        }

        public async Task<IEnumerable<Message>> GetLastMessagesAsync(string room, DateTime from)
        {
            var query = 
                new TableQuery<ChatMessageEntity>().
                    Where(
                        TableQuery.CombineFilters(
                            TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, room),
                            TableOperators.And,
                            TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.GreaterThanOrEqual, from.ToString())));

            TableContinuationToken token = null;

            var result = new List<ChatMessageEntity>();

            do
            {
                var segment = await table.ExecuteQuerySegmentedAsync(query, token);

                token = segment.ContinuationToken;

                result.AddRange(segment);
            } while (token != null);

            return result.Select(entity => entity.CreateMessage());
        }
    }
}
