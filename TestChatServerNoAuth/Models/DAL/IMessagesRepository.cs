﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestChat.Infrastructure;

namespace TestChatServerNoAuth.Models.DAL
{
    public interface IMessagesRepository
    {
        /// <summary>
        /// Добавить сообщение
        /// </summary>
        /// <param name="message">Новое сообщение</param>
        /// <returns></returns>
        Task Add(Message message);

        /// <summary>
        /// Получить сообщения из комнаты, начиная с определенной даты
        /// </summary>
        /// <param name="room">Комната</param>
        /// <param name="from">Дата</param>
        /// <returns></returns>
        Task<IEnumerable<Message>> GetLastMessagesAsync(string room, DateTime from);
    }
}
