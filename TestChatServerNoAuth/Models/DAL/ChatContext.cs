﻿using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestChatServerNoAuth.Models.DAL
{
    public class ChatContext : IChatContext
    {
        public ChatContext(string connectionStringName)
        {
            // Подключение к Azure Table Storage
            // строку подключения смотреть в Startup во время DI
            var storageAccount = CloudStorageAccount.Parse(
                CloudConfigurationManager.GetSetting(connectionStringName));

            var tableClient = storageAccount.CreateCloudTableClient();

            var messagesTable = tableClient.GetTableReference("messages");
            messagesTable.CreateIfNotExists();
            this.MessagesRepository = new MessagesRepository(messagesTable);
        }


        public IMessagesRepository MessagesRepository { get; set; }
    }
}
