﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestChatServerNoAuth.Models.DAL
{
    public interface IChatContext
    {
        IMessagesRepository MessagesRepository { get; set; }
    }
}
