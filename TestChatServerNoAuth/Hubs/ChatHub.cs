﻿using System;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.WindowsAzure.Storage;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage.Table;
using TestChatServerNoAuth.Models.DAL;
using TestChatServerNoAuth.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestChat.Infrastructure;

namespace SignalRChat
{
    /// <summary>
    /// Хаб чата
    /// </summary>
    public class ChatHub : Hub<IChatClient>, IChatService
    {
        private IMessagesRepository repository;

        private static Dictionary<string, Dictionary<string, string>> rooms
            = new Dictionary<string, Dictionary<string, string>>();


        public ChatHub(IMessagesRepository repository)
        {
            if (repository == null)
            {
                throw new ArgumentNullException();
            }

            this.repository = repository;
        }

        /// <summary>
        /// При отключении выходим из всех комнат
        /// </summary>
        /// <param name="stopCalled"></param>
        /// <returns></returns>
        public override async Task OnDisconnected(bool stopCalled)
        {
            await this.LeaveAllRoomsAsync();

            await base.OnDisconnected(stopCalled);
        }

        public async Task JoinRoomAsync(string room, string user)
        {
            if ((room == null) || (user == null))
            {
                return;
            }

            var connection = Context.ConnectionId;

            await Groups.Add(connection, room);

            if (rooms.ContainsKey(room))
            {
                if (!rooms[room].ContainsKey(connection))
                {
                    rooms[room][connection] = user;
                }
            }
            else
            {
                rooms[room] = new Dictionary<string, string>()
                {
                    { connection, user }
                };
            }

            await this.SendAsync(
                new Message()
                {
                    User = "Room " + room + " notification",
                    Room = room,
                    DateTime = DateTime.UtcNow,
                    Text = String.Format("User {0} has joined.", user)
                });
        }

        public async Task LeaveRoomAsync(string room)
        {
            if (room == null)
            {
                return;
            }

            var connection = Context.ConnectionId;

            if (rooms.ContainsKey(room))
            {
                if (rooms[room].ContainsKey(connection))
                {
                    var user = rooms[room][connection];

                    await this.SendAsync(
                        new Message()
                        {
                            User = "Room " + room + " notification",
                            Room = room,
                            DateTime = DateTime.UtcNow,
                            Text = String.Format("User {0} has leaved.", user)
                        });

                    rooms[room].Remove(connection);
                }

                if (rooms.Count == 0)
                {
                    rooms.Remove(room);
                }
            }

            await Groups.Remove(connection, room);
        }

        private async Task LeaveAllRoomsAsync()
        {
            foreach (var room in new List<string>(rooms.Keys))
            {
                await this.LeaveRoomAsync(room);
            }
        }

        public async Task SendAsync(Message message)
        {
            message.DateTime = DateTime.UtcNow;

            await this.repository.Add(message);

            // Call the addNewMessage method to update clients.
            Clients.Group(message.Room).AddNewMessage(message);
        }

        public async Task<IEnumerable<Message>> GetLastMessagesAsync(string room, DateTime from)
        {
            return await this.repository.GetLastMessagesAsync(room, from);
        }

        public async Task<IEnumerable<Message>> GetTodayMessagesAsync(string room)
        {
            return await this.repository.GetLastMessagesAsync(room, DateTime.UtcNow.Date);
        }
    }
}