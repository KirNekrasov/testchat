﻿using Owin;
using Microsoft.Owin;
using Microsoft.AspNet.SignalR;
using TestChatServerNoAuth.Models.DAL;
using TestChatServerNoAuth.Controllers;

[assembly: OwinStartup(typeof(SignalRChat.Startup))]
namespace SignalRChat
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // Строка подключения берется из web.config
            // Подключаемся к эмулятору Azure Table Storage
            GlobalHost.DependencyResolver.Register(
                typeof(ChatHub), () => new ChatHub(new ChatContext("StorageConnectionString").MessagesRepository));

            // Any connection or hub wire up and configuration should go here
            app.MapSignalR();
        }
    }
}